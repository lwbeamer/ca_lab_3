.data:
    num sum 0
    num max 100
    num cur_num 1

.text:
loop:   add cur_num sum
        cmp cur_num max
        je end
        add 1 cur_num
        jmp loop
end:    out sum
        hlt
